var express = require('express');
var router = express.Router();
var listingController = require('../controllers/listingController.js');


router.get('/', listingController.list);
router.get('/:id', listingController.show);
router.post('/', listingController.create); 
router.put('/:id', listingController.update);
router.delete('/:id', listingController.remove);

module.exports = router;
