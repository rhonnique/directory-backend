var express = require('express');
var router = express.Router();
var listingController = require('../controllers/listingController.js');

router.get('/listings', listingController.list);
router.get('/listings/:slug', listingController.show);

module.exports = router;