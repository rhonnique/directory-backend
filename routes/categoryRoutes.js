var express = require('express');
var router = express.Router();
var categoryController = require('../controllers/categoryController.js');


router.get('/get', categoryController.list);
router.get('/', categoryController.list);
router.get('/:id', categoryController.show);
router.post('/', categoryController.create);
router.put('/:id', categoryController.update);
router.delete('/:id', categoryController.remove);
module.exports = router;
