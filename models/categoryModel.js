var mongoose = require('mongoose');
const title = require('title');
const slugify = require('slugify');
var Schema = mongoose.Schema;

var categorySchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	slug: String,
	status: {
		type: Boolean,
		default: true
	},
	created_at: {
		type: Date,
		default: Date.now
	},
	updated_at: {
		type: Date,
		default: Date.now
	}
});

categorySchema.pre('save', function (next) {
	const now = new Date();
	this.updated_at = now;
	this.name = title((this.name).trim(), {
		special: [ 'ICT', 'IT', 'CCTV']
	  });
	this.slug = slugify((this.name).toLowerCase());
	next();
});

module.exports = mongoose.model('category', categorySchema);
