var mongoose = require('mongoose');
const title = require('title')
var Schema = mongoose.Schema;
const slugify = require('slugify');

var listingSchema = new Schema({
	business_name: {
		type: String,
		required: true,
		// unique: true
	},
	slug: String,
	categories: [{
        category: {
            type: Schema.Types.ObjectId,
            ref: 'category'
        }
	}],
	email : {
		type: String,
		required: true,
		// unique: true
	},
	phone: {
		type: String,
		required: true
	},
	address: {
		type: String,
		required: true
	},
	website: {
		type: String
	},
	description: {
		type: String
	},
	images: [],
	views: {
		type: Number,
		default: 0
	},
	status: {
		type: Boolean,
		default: true
	},
	created_at: {
		type: Date,
		default: Date.now
	},
	updated_at: {
		type: Date,
		default: Date.now
	}
});

listingSchema.pre('save', async function (next) {
	const now = new Date();
	this.updated_at = now;
	this.business_name = title((this.business_name).trim());
	this.slug = slugify((this.business_name).toLowerCase());
	this.email = (this.email).toLowerCase().trim();
	next();
});

module.exports = mongoose.model('listing', listingSchema);
