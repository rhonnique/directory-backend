var listingModel = require('../models/listingModel.js');
var mongoose = require('mongoose');
const upload = require("../middleware/upload");

/**
 * listingController.js
 *
 * @description :: Server-side logic for managing listings.
 */
module.exports = {

    /**
     * get backend
     */
    list: function (req, res) {
        listingModel.find()
            .populate({ path: 'categories.category', select: 'name' })
            .sort({ "created_at": -1 })
            .exec(function (err, results) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting listing.',
                        error: err
                    });
                }
                return res.json(results);
            });
    },

    /**
     * mealController.show()
     */
    show: function (req, res) {
        var slug = req.params.slug;
        listingModel.findOne({ slug: slug })
            .populate({ path: 'categories.category', select: 'name' })
            .exec(function (err, listing) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when getting listing.',
                        error: err
                    });
                }

                if(listing){
                    listing.views = (listing.views + 1);
                    listing.save();
                }
                       
                return res.json(listing);
            });
    },

    /**
     * mealController.create()
     */
    create: async function (req, res) {
        let images = [];
        try {
            await upload(req, res);
            //console.log(req.files);

            if (req.files.length <= 0) {
                return res.status(500).json({
                    message: 'You must select at least 1 file.'
                });
            }

            images = req.files.map(image => image.filename);
            //return res.status(200).json(images);
        } catch (error) {
            console.log(error);

            if (error.code === "LIMIT_UNEXPECTED_FILE") {
                return res.status(500).json({
                    message: 'Maximum file upload reached'
                });
            }

            //return res.send(`Error when trying upload many files: ${error}`);
            return res.status(500).json({
                message: error
            });
        }

        if (images.length <= 0) {
            return res.status(500).json({
                message: 'No image uploaded'
            });
        }

        const formatCats = category => ({ category: mongoose.Types.ObjectId(category) });
        const splitCats = req.body.categories.split(',');
        const categories = splitCats.map(formatCats);

        var data = new listingModel({
            business_name: req.body.business_name,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address,
            website: req.body.website ? req.body.website : '',
            description: req.body.description ? req.body.description : '',
            images: images,
            categories: categories
        });

        data.save(function (err, results) {
            if (err) {
                console.log(err, 'data.save...');
                return res.status(500).json({
                    message: 'An error occurred, please retry',
                    error: `${err}`
                });
            }
            return res.status(200).json(results);
        });

    },

    /**
     * mealController.update()
     */
    update: async function (req, res) {
        let images = [];
        //var id = req.body.id;
        var id = req.params.id;
        try {
            await upload(req, res);
            if (req.files.length > 0) {
                images = req.files.map(image => image.filename);
            }
        } catch (error) {
            if (error.code === "LIMIT_UNEXPECTED_FILE") {
                return res.status(500).json({
                    message: 'Maximum file upload reached'
                });
            }

            return res.status(500).json({
                message: error
            });
        }

        listingModel.findOne({ _id: id }, function (err, listing) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when fetching data',
                    error: err
                });
            }
            if (!listing) {
                return res.status(404).json({
                    message: 'Listing not found'
                });
            }

            const current_images = req.body.current_images.split(',');
            let new_images = images.length > 0 && current_images.length > 0 ? [...current_images, ...images] : current_images;
            new_images = new_images.filter(Boolean);

            const formatCats = category => ({ category: mongoose.Types.ObjectId(category) });
            const splitCats = req.body.categories.split(',');
            const categories = splitCats.map(formatCats);

            listing.business_name = req.body.business_name ? req.body.business_name : listing.business_name;
            listing.email = req.body.email ? req.body.email : listing.email;
            listing.phone = req.body.phone ? req.body.phone : listing.phone;
            listing.address = req.body.address ? req.body.address : listing.address;
            listing.website = req.body.website ? req.body.website : listing.website;
            listing.description = req.body.description ? req.body.description : listing.description;
            listing.categories = req.body.categories ? categories : listing.description;
            listing.images = new_images;

            listing.save(function (err, results) {
                if (err) {
                    return res.status(500).json({
                        message: 'An error occurred, please retry',
                        error: `${err}`
                    });
                }

                return res.json(results);
            });
        });


    },



    /**
     * mealController.update()
     */
    updatemm: async function (req, res) {
        var id = req.params.id;
        let images = [];

        try {
            await upload(req, res);
            // console.log(req.files);

            if (req.files.length > 0) {
                images = req.files.map(image => image.filename);
            }


        } catch (error) {
            if (error.code === "LIMIT_UNEXPECTED_FILE") {
                return res.status(500).json({
                    message: 'Maximum file upload reached'
                });
            }

            //return res.send(`Error when trying upload many files: ${error}`);
            return res.status(500).json({
                message: error
            });
        }

        const formatCats = category => ({ category: mongoose.Types.ObjectId(category) });
        const splitCats = req.body.categories.split(',');
        const categories = splitCats.map(formatCats);

        var data = new listingModel({
            business_name: req.body.business_name,
            email: req.body.email,
            phone: req.body.phone,
            address: req.body.address,
            website: req.body.website ? req.body.website : '',
            description: req.body.description ? req.body.description : '',
            images: images,
            categories: categories
        });

        data.save(function (err, results) {
            if (err) {
                console.log(err, 'data.save...');
                return res.status(500).json({
                    message: 'An error occurred, please retry',
                    error: `${err}`
                });
            }
            return res.status(200).json(results);
        });

    },

    /**
     * mealController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        listingModel.findByIdAndRemove(id, function (err, results) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting data.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    },

    arrangeMenu: async function (req, res) {
        /* let restaurant_id = req.body.selectedVendorId;
        let menu_list = req.body.menuList;

        mealModel.find({ restaurant: mongoose.Types.ObjectId(restaurant_id) })
            .exec(function (err, results) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error getting meals',
                        error: err
                    });
                }

                let total_updated = 0;
                let total_failed = 0;

                results.map(async (meal, index) => {
                    let newIndex = menu_list.indexOf(`${meal.category}`);

                    newIndex = (newIndex + 1);
                    const update = await mealModel.findByIdAndUpdate(meal._id, { sorts: newIndex }).exec();

                    if (update) {
                        total_updated++;
                    } else {
                        total_failed++;
                    }
                });
                console.log({ total_updated, total_failed }, 'updated...');
                return res.status(201).json({ total_updated, total_failed });
            }); */

    },

};
