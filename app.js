var createError = require('http-errors');
var express = require('express');
const cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser  = require('body-parser');
const morgan = require('morgan');
const winston = require('./winston/config');

/*
  Mongo db to be connected here
  ALso set to global
*/
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);


mongoose.connect('mmongodb://localhost:27017/directory', { useNewUrlParser: true })
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));


const app = express()
const port = 8888
app.use(morgan('combined', { stream: winston.stream }));
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var indexRouter = require('./routes/index');
var categoryRouter = require('./routes/categoryRoutes.js');
var listingRouter = require('./routes/listingRoutes.js');
var frontendRouter = require('./routes/frontendRoutes.js')

app.use('/', indexRouter);
app.use('/api/backend/category', categoryRouter);
app.use('/api/backend/listing', listingRouter);
app.use('/api', frontendRouter);

app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, () => console.log(`listening on port ${port}!`))