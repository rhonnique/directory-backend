const util = require("util");
const path = require("path");
const multer = require("multer");
var randomstring = require("randomstring");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, 'public/uploads/');
  },
  filename: (req, file, callback) => {
    const match = ["image/png", "image/jpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      return callback(message, null);
    }

    var strfile = randomstring.generate({
      length: 20,
      charset: 'alphabetic'
    });

    var ext = file.originalname.split('.').pop();
    //var filename = `${Date.now()}-file-${file.originalname}`;
    var filename = `${strfile}.${ext}`;
    callback(null, filename);
  }
});

var uploadFiles = multer({ storage: storage }).array("images", 10);
var uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;